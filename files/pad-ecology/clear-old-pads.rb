#!/usr/bin/env ruby
# encoding: utf-8
#
# Destroys old pads. Should be run regularly.
#
# Requirements:
#
#   sudo apt-get install ruby ruby-mysql2
#
# Usage
#
#   ./clear-old-pad.rb
#
#   ./clear-old-pad.rb --verbose
#
#   ./clear-old-pad.rb --dryrun
#

require 'json/pure'
require 'stringio'
require 'mysql2'

##
## CONFIGURATION
##

$default_time_to_live = '61 day'
$keep_time_to_live    = '1 year'
$tmp_time_to_live    = '2 day'
$keep_suffix = '-keep'
$tmp_suffix = '-tmp'
$settings_path = '/srv/etherpad-lite/settings.json'

##
## PARSING
## This is very crude config parsing. It will bomb out if
## the json file has comments on lines that need to be parsed.
##

begin
  buffer = StringIO.new
  File.open($settings_path) do |f|
    while line = f.gets 
      if line =~ /\/\*/
        while line !~ /\*\//
          line = f.gets
        end
      end
      if line !~ /\/\// && line !~ /\*\//
        buffer << line
      end
    end
  end
  $settings = JSON.parse(buffer.string)['dbSettings']
rescue StandardError => exc
  puts 'ERROR: could not load etherpad settings file'
  puts exc.to_s
  exit 1
end
   
$db_host = $settings['host']
$db_user = $settings['user']
$db_db   = $settings['database']
$db_pass = $settings['password']
$dryrun  = !!ARGV.delete('--dryrun')
$verbose = !!ARGV.delete('--verbose') || $dryrun

unless ARGV.empty?
  puts "ERROR: could not understand arguments"
  exit 1
end

##
## MAIN
##

def main()
  $my = Mysql2::Client.new(
    host: $db_host, username: $db_user,
    password: $db_pass, database: $db_db
  )
  pads = pads_to_destroy()
  pads.each_with_index do |pad, i|
    begin
      $my.query("BEGIN") unless $dryrun
      print_prefix(i+1, pads.length)
      destroy_pad(pad)
      print_prefix(i+1, pads.length, ' ')
      destroy_pad_timestamps(pad)
      $my.query("COMMIT") unless $dryrun
    rescue StandardError => e
      if e.to_s == "padID does not exist"
        destroy_pad_timestamps(pad)
      else
        puts "encountered error: #{e.to_s}"
        puts "nevertheless, moving on..."
      end
    end
    sleep 0.2
  end
end

##
## HELPERS
##

def pads_to_destroy
  pads = []
  result = $my.query(
    "SELECT pad, time FROM pad_timestamps " +
    "WHERE time < DATE_SUB(NOW(), INTERVAL #{$default_time_to_live}) " +
    "AND pad COLLATE UTF8MB4_UNICODE_CI NOT LIKE '%#{$keep_suffix}'"
  )
  result.each {|row| pads << row if row["pad"] && !row["pad"].empty?}
  result = $my.query(
    "SELECT pad, time FROM pad_timestamps " +
    "WHERE time < DATE_SUB(NOW(), INTERVAL #{$keep_time_to_live}) " +
    "AND pad COLLATE UTF8MB4_UNICODE_CI LIKE '%#{$keep_suffix}'"
  )
  result.each {|row| pads << row if row["pad"] && !row["pad"].empty?}
  result = $my.query(
    "SELECT pad, time FROM pad_timestamps " +
    "WHERE time < DATE_SUB(NOW(), INTERVAL #{$tmp_time_to_live}) " +
    "AND pad COLLATE UTF8MB4_UNICODE_CI LIKE '%#{$tmp_suffix}'"
  )
  result.each {|row| pads << row if row["pad"] && !row["pad"].empty?}
  puts "found #{pads.length} pads to destroy" if $verbose
  return pads
end

#
# escape string for use with = conditions (AND ONLY EQUALS CONDITIONS)
# wildcards must NOT be escaped!
#
def escape_string(str)
  $my.escape(str)
end

#
# escape string for use with LIKE conditions (AND ONLY LIKE CONDITIONS)
# wildcards must be escaped too!
#
def escape_string_like(str)
  $my.escape(str).gsub('%', '\%').gsub('_', '\_')
end

def destroy_pad(pad)
  pad_name = escape_string(pad["pad"])
  pad_like = escape_string_like(pad["pad"])
  if $verbose
    print "destroying: #{pad_name} (#{pad["time"]})"
    STDOUT.flush
  end
  if !$dryrun
    $my.query("DELETE FROM store WHERE `key` = 'pad:#{pad_name}'")
    $my.query("DELETE FROM store WHERE `key` = 'pad2readonly:#{pad_name}'")
    $my.query("DELETE FROM store WHERE `key` LIKE 'pad:#{pad_like}:%'")
    $my.query("DELETE FROM store WHERE `key` LIKE 'pad2readonly:#{pad_like}:%'")
  end
  puts '.' if $verbose
end

def destroy_pad_timestamps(pad)
  pad_name = escape_string(pad["pad"])
  if $verbose
    print "clear timestamps: #{pad_name}"
    STDOUT.flush
  end
  if !$dryrun
    $my.query("DELETE FROM pad_timestamps WHERE pad = '#{pad_name}'")
  end
  puts '.' if $verbose
end

def print_prefix(i, length, prefix=nil)
  if $verbose
    str = "[%i/%i] " % [i,length]
    if prefix == nil
      print str
    else
      print prefix * str.length
    end
  end
end

main()
