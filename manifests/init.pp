# etherpad_lite
class etherpad_lite (
  $db_name = 'etherpad',
  $db_user = 'etherpad',
  $db_host = '127.0.0.1',
  $db_password = 'absent',
  $nodejs_version = 'installed',
  $settings_content = 'absent',
  $settings_source = 'absent',
  $repo = 'git://github.com/ether/etherpad-lite.git',
  $repo_ensure = 'present',
  $repo_rev = 'master',
  $ensure = 'present',
  $clear_pads = 'absent',
  $api_url = 'absent',
  $api_key = 'absent',
  $use_nagios = true
) {

  # debian's nodejs packages will not get security updates again
  # https://www.debian.org/releases/stable/amd64/release-notes/ch-information.en.html#libv8

  # There are 3 current nodejs versions, 0.6 (on lts mode), 0.8 and 0.10.
  # 0.8 is the current one, maintenance will start on October 2017 and end
  # on December 2019
  apt::source { 'nodesource':
    comment  => 'Node',
    location => '[signed-by=/usr/share/keyrings/nodesource.gpg] https://deb.nodesource.com/node_10.x',
    repos    => 'main',
    release  => $lsbdistcodename,
    require  => [ Apt::Pin['nodejs_negative'], File['/usr/share/keyrings/nodesource.gpg'] ]
  }

  apt::pin {
    'nodejs':
      packages   => 'nodejs',
      priority   => 1001,
      originator => 'Node Source',
      component  => 'main',
      label      => 'Node Source';
    'nodejs_negative':
      packages   => '*',
      priority   => 400,
      originator => 'Node Source',
      component  => 'main',
      label      => 'Node Source';
  }

  file {
    '/usr/share/keyrings/nodesource.gpg':
      ensure => present,
      source => 'puppet:///modules/site_apt/keys/nodesource.gpg',
      owner  => 'root', group => 0, mode => '644';
  }


  package { [ 'abiword', 'curl', 'wget', 'ruby-mysql2' ]:
    ensure => installed;
  }

  package { [ 'nodejs' ]:
    ensure  => $nodejs_version,
    require => Apt::Source['nodesource']
  }

  mysql_database { $db_name:
    ensure  => $ensure,
    collate => 'utf8mb4_bin',
    charset => 'utf8mb4'
  }

  $mysql_password = $db_password ? {
    'trocla' => trocla("mysql_${db_user}",'mysql'),
    default  => mysql_password($db_password)
  }

  if $db_password == 'absent' and $ensure != 'absent' {
    fail('You need to define the etherpad-lite database password')
  } else {
    mysql_user { "${db_user}@${db_host}":
      ensure        => $ensure,
      password_hash => $mysql_password,
      require       => Mysql_database[$db_name]
    }
  }

  # This doesn't work with the new mysql module, because the database name having a - in it
  #if $ensure == 'present' {
  #  mysql_grant{"${db_user}@${db_host}/${db_name}":
  #    user       => "${db_user}@${db_host}",
  #    table      => "${db_name}",
  #    privileges => [ 'alter', 'create', 'select', 'insert',
  #                    'update', 'delete', 'trigger' ],
  #    require    => [ Mysql_database[$db_name],
  #                    Mysql_user["${db_user}@${db_host}"] ];
  #  }
  #}

  group { 'etherpad-lite':
    ensure    => $ensure,
    allowdupe => false,
  }

  user { 'etherpad-lite':
    ensure    => $ensure,
    allowdupe => false,
    gid       => 'etherpad-lite',
    require   => Group['etherpad-lite'],
  }

  file { '/srv/etherpad-lite':
    ensure  => directory,
    owner   => 'etherpad-lite',
    group   => 'etherpad-lite',
    require => User['etherpad-lite'],
  }

  vcsrepo { '/srv/etherpad-lite':
    ensure   => $repo_ensure,
    provider => git,
    source   => $repo,
    revision => $repo_rev,
    owner    => 'etherpad-lite',
    group    => 'etherpad-lite',
    require  => [ User['etherpad-lite'], Group['etherpad-lite'] ],
    notify   => Service['etherpad-lite'],
  }

  file {
    '/var/log/etherpad-lite':
      ensure  => directory,
      owner   => 'etherpad-lite',
      group   => 'etherpad-lite',
      mode    => '0755',
      require => [ User['etherpad-lite'], Group['etherpad-lite'] ];

    '/srv/etherpad-lite/settings.json':
      ensure  => $ensure,
      mode    => '0640',
      owner   => etherpad-lite,
      group   => etherpad-lite,
      notify  => Service['etherpad-lite'],
      require => Vcsrepo['/srv/etherpad-lite'];

    '/srv/etherpad-lite/APIKEY.txt':
      ensure  => $ensure,
      content => template('etherpad_lite/APIKEY.txt.erb'),
      mode    => '0644',
      owner   => etherpad-lite,
      group   => etherpad-lite,
      notify  => Service['etherpad-lite'],
      require => Vcsrepo['/srv/etherpad-lite'];
  }

  case $settings_content {
    'absent': {
      $real_settings_source = $settings_source ? {
        'absent' => [
                      'puppet:///modules/site_etherpad/configs/settings.json',
                      'puppet:///modules/etherpad_lite/configs/settings.json'
                    ],
        default => "puppet:///modules/${settings_source}",
      }
      File['/srv/etherpad-lite/settings.json']{
        source => $real_settings_source,
      }
    }
    default: {
      File['/srv/etherpad-lite/settings.json']{
        content => $settings_content,
      }
    }
  }

  file { '/etc/systemd/system/etherpad-lite.service':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => 'puppet:///modules/etherpad_lite/etherpad-lite.service',
      require => Vcsrepo['/srv/etherpad-lite'];
  }

  file { '/etc/rsyslog.d/etherpad-lite.conf':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => 'puppet:///modules/etherpad_lite/etherpad-lite.rsyslog',
      require => Vcsrepo['/srv/etherpad-lite'];
  }

  service { 'etherpad-lite':
    provider   => 'systemd',
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
    require    => [ File[ '/srv/etherpad-lite/settings.json',
                          '/srv/etherpad-lite',
                          '/etc/systemd/system/etherpad-lite.service'] ]

# removed because of grant problem commented out earlier
#                    Mysql_grant["${db_user}@${db_host}/${db_name}" ] ]
  }

  if $use_nagios {
    check_mk::agent::ps {
      'etherpad':
        procname => '~/usr/bin/nodejs /srv/etherpad-lite/node_modules/ep_etherpad-lite/node/server\.js';
    }
  }

  augeas {
    'logrotate_etherpad':
      context => '/files/etc/logrotate.d/etherpad-lite/rule',
      changes => [  'set file /var/log/etherpad-lite/*.log','set rotate 5',
                    'set schedule daily', 'set compress compress',
                    'set delaycompress delaycompress', 'set ifempty notifempty',
                    'set copytruncate copytruncate', 'set create/mode 0640',
                    'set create/owner etherpad-lite',
                    'set create/group etherpad-lite' ]
  }

  if $clear_pads != 'absent' {

    file {
      '/usr/local/bin/clear-old-pads.rb':
        source => 'puppet:///modules/etherpad_lite/pad-ecology/clear-old-pads.rb',
        mode   => '0755',
        owner  => etherpad-lite,
        group  => etherpad-lite;
    }

    cron { 'pad_ecology':
      command => '/usr/local/bin/clear-old-pads.rb 2>/dev/null',
      user    => etherpad-lite,
      hour    => $clear_pads,
      minute  => 0;
    }
  }
}

